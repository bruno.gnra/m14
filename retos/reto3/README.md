RETO 3
========
Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge postgres original amb populate i persistència de dades. Exemples de SQL injectat usant volumes.

## Imagen postgres oficial

```
/library/postgres

```
[docker official images](https://hub.docker.com/_/postgres)

## Encendemos el container para desplegar el postgres 

```
$ docker build -t brunora/m14_project:reto3 .

$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd)/training:/docker-entrypoint-initdb.d -v postgres-data:/var/lib/postgresql/data --net 2hisx -d brunora/m14_project:reto3

```

## Comprobamos el funcionamiento de la BBDD 

Ejecutamos el comando dentro del container para conectarnos a PostgreSQL.

```
$ docker exect -it training psql -d training -U postgres 

```
Listamos las tablas de BD training.
```
\d 
```
Borramos una tabla cualquiera, en este caso ***pedidos***.

```
DROP TABLE pedidos; 

\d 
```
Salimos del container y lo paramos.

```
CTRL+d

$ docker stop training

```

## Persistencia de datos 

Volvemos a encender el container a partir del mismo docker volumen anterior, ***postgres-data***.  
Quitamos la parte ***-v $(pwd)/training:/docker-entrypoint-initdb.d***  ya que la BD no esta vacia, por lo tanto no realizara la inicializacion (no ejecutara cualquier script del directorio training).

```
$ docker run --rm --name training -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres-data:/var/lib/postgresql/data --net 2hisx -d brunora/m14_project:reto3
```

Nos conectamos a postgres para comprobar que efectivamente continua estando eliminada la tabla pedidos.  
Comprobamos tambien desde el localhost 

```
$ docker exect -it training psql -d training -U postgres 

\d 


$ psql -h 172.18.0.2 -U postgres -d training -c "SELECT * FROM pedidos;" 
```
