# RETO 4

Implementar una imatge que actui com a servidor lda pamb persistència de dades tant de configuració com les dades de la base de dades d’usuaris.

## Creamos el directorio con los ficheros necesarios para crear la imagen de ldap.

```
$ pwd
$ mkdir ldap
$ cd ldap 
```
## Creamos la imagen y encendemos el container **sin persistencia de datos**.

```
$ docker build -t brunora/m14_project:reto4 .

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d brunora/m14_project:reto4
```
## Comprobamos que el servidor ldap va y tiene datos, cualquier modificación se perdera al parar el container.

```
$ ldapsearch -x -LLL -b 'dc=edt,dc=org' db 

$ ldapdelete -vxr -D 'cn=manager,dc=edt,dc=org' -w secret 'cn=Marta Mas,ou=usuaris,dc=edt,dc=org'

$ ldapsearch -x -LLL -b 'dc=edt,dc=org' db 

$ docker stop ldap.edt.org 

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d brunora/m14_project:reto4

```

## Paramos y encendemos nuevamente el container pero guardando en dos volúmenes por un lado los datos de configuración y por otro los datos LDAP.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d brunora/m14_project:reto4
```

Si los volumenes no estan creados previamente, se crean automaticamente con **-v**.  
**ldap-config** destinado a guardar los cambios en el directorio de configuracion: */etc/ldap/slapd.d* y **ldap-data** destinado a guardar los cambios en el directorio de datos: */var/lib/ldap*

## Comprobaciones 
```
$ ldapsearch -x -LLL -b 'dc=edt,dc=org' db 

```
