# RETO 5

## Editamos el Dockerfile cambiando el CMD por ENTRYPOINT.
```
ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
```
## Construimos el script startup.sh

## Creamos la imagen base 
```
$ docker build -t brunora/m14_project:reto5 .
```
## initdb  

Encendemos el container con el servidor ldap con datos iniciales y comprobamos los datos cargados.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d brunora/m14_project:reto5 initdb

$ ldapsearch -x -LLL -b 'dc=edt,dc=org'
```
## slapd

Encendemos el container con el servidor ldap vacio.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d brunora/m14_project:reto5 slapd

```
Visualizamos la configuracion y comprobamos que los datos no  estan cargados. 

```
$ docker exec -it ldap.edt.org slapcat -n0

$ ldapsearch -x -LLL -b 'dc=edt,dc=org'
```

## slapcat

Encendemos el container interactivamente mostrando unicamente los datos, seguidamente se elimina el container. 

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it brunora/m14_project:reto5 slapcat

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it brunora/m14_project:reto5 slapcat 0

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it brunora/m14_project:reto5 slapcat 1
```

## start|edt.org|null

Ponemos en marcha el servidor utilizando la persistencia de datos de la BD y de la configuración. Levantamos el servicio usando los datos ya existentes.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it brunora/m14_project:reto5 start
 
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it brunora/m14_project:reto5 edt.org

$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it brunora/m14_project:reto5
```

Comprobamos los datos cargados, realizamos alguna modificación para finalmente para el container y volver a levantarlo cambiando el argumento.

```
$ ldapsearch -x -LLL -b 'dc=edt,dc=org'
 
$ ldapdelete -vxr -D 'cn=manager,dc=edt,dc=org' -w secret 'cn=Marta Mas,ou=usuaris,dc=edt,dc=org'

$ docker stop ldap.edt.org
```
