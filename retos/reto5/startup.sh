#! /bin/bash
#
# @BrunoRodriguezAranibar
# 2hisx M14_proyectos:reto5 
# Script para configurar el servidor ldap segun el argumento recibido. 
# 
arg=$1 
OK=0

# validamos # arg
if [ $# -eq 0 ]
then
	arg="null"
fi 

case $arg in 
  "initdb")
	echo "Servidor ldap con datos iniciales"
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
	slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
	;;
  "slapd") 
	echo "Servidor ldap vacio" 
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
	;;
  "slapcat") 
	echo "$arg $2"
	if [ $# -lt 2 ]
	then
	  slapcat 
	else 
	  slapcat -n$2
	fi
	;;
  "start"|"edt.org"|"null")
	echo "opcion $arg"
	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
	/usr/sbin/slapd -d0
	;;
esac

exit $OK

