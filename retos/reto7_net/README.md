# RETO 7 NET

Implementa els serveis de xarxa echo, daytime, chargen, ftp, tftp i http.  

## Creamos el directorio y los ficheros de configuración:

```
$ ls 

chargen  daytime  Dockerfile  echo  index.html  README.md  startup.sh

```
```
$ cat Dockerfile

# Servidor Net
FROM debian:latest
LABEL version="1.0"
LABEL author="@Bruno Rodriguez"
LABEL subject="Server net"
RUN apt-get update 
RUN apt-get -y install procps iproute2 tree nmap vim xinetd vsftpd apache2
RUN mkdir /opt/docker
COPY ./* /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 7 13 19 21 80

```
Utilizaremos el daemon xinetd, este mismo nos permitira el acceso a los subservicios echo(7),daytime(13),chargen(19). 

Editaremos los ficheros de configuracion para cada uno de los servicios, localizados en /etc/xinetd.d/ cambiando el valor de la variable con disable = no. Por defecto encontraremos dos ficheros de cada servicio segun su protocolo a usar "tcp/udp" atacaremos a los de tipo tcp. 

```
$ tail -n11 daytime echo chargen 

==> daytime <==
# This is the tcp version.
service daytime
{
	disable		= no
	type		= INTERNAL
	id		= daytime-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}

==> echo <==
# This is the tcp version.
service echo
{
	disable		= no
	type		= INTERNAL
	id		= echo-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}

==> chargen <==
# This is the tcp version.
service chargen
{
	disable		= no
	type		= INTERNAL
	id		= chargen-stream
	socket_type	= stream
	protocol	= tcp
	user		= root
	wait		= no
}
```
Para los servicios ftp, http usaremos el paquete vsftpd y apache2.

## Creamos la imagen y levantamos el container 

```
$ docker build -t brunora/m14_project:reto7_net .

$ docker run --rm --name net.edt.org -h net.edt.org --net 2hisx -p 7:7 -p 13:13 -p 19:19 -p 21:21 -p 80:80 -d brunora/m14_project:reto7_net

```

## Comprobaciones 

Listamos los puertos activos:

```
$ docker exec -it net.edt.org nmap localhost

Starting Nmap 7.80 ( https://nmap.org ) at 2022-10-25 22:56 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000070s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 995 closed ports
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
21/tcp open  ftp
80/tcp open  http

```

Comprobamos los servicios: 

```
$ telnet localhost 7

Trying ::1...
Connected to localhost.
Escape character is '^]'.
hala
hala
Madrid
Madrid
^]
telnet> q
Connection closed.

```
```
$ telnet localhost 13

Trying ::1...
Connected to localhost.
Escape character is '^]'.
25 OCT 2022 22:59:58 UTC
Connection closed by foreign host.

```

```
$ telnet localhost 19

Trying ::1...
Connected to localhost.
Escape character is '^]'.
ijklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRS
jklmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRST
klmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTU
lmnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUV
mnopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVW
nopqrstuvwxyz{|}!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWX
opqrstuvwxyz{|}!"#$%&'()*+,-.
telnet> q
Connection closed.
```


