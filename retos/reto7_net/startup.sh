#! /bin/bash

# Encendemos el servicio apache2 
/etc/init.d/apache2 start

# copiamos el html de nuestra web 
cp index.html /var/www/html/

# Levantamos el servicio ftp
/etc/init.d/vsftpd start

# Copiamos los ficheros de configuración al directorio /etc/xinetd.d/
cp echo daytime chargen /etc/xinetd.d/

# Levantamos el servicio xinetd
# /etc/init.d/xinetd start
/usr/sbin/xinetd -dontfork

