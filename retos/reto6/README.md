# RETO6

Implementar un servidor LDAP on el nom de l’administrador de la base de dades de l’escola sigui captat a través de variables d’entorn.

## vim startup.sh
Editamos el script startup.sh de tal manera que remplazaremos los valores declarados en el slapd.conf por los que definamos a traves del comando docker run.  
  
Añadimos las siguientes lineas: 
```
sed -i "s/Manager/$ADMIN_NAME/" slapd.conf
sed -i "s/secret/$ADMIN_PASSWD/" slapd.conf
```

## Variables de Entorno
Ponemos en marcha el container declarando las variables $ADMIN_NAME y $ADMIN_PASSWD:

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -e ADMIN_NAME="bruno" -e ADMIN_PASSWD="bruno" -d brunora/m14_project:reto6
```

## Comprobaciones 

```
docker exec -it ldap.edt.org cat slapd.conf
```
