provider "aws" {
  region     = "eu-west-2"
  access_key = "AKIAQUYHMRZWYXAIF4A2"
  secret_key = "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"
}

terraform {
required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = ">= 2.0.0"
    }
}
}

resource "aws_instance" "example" {
  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
  instance_type = "t2.micro" // replace with your desired instance type

  // Provision the instance with Docker Engine
  user_data = <<-EOF
              #!/bin/bash
              sudo amazon-linux-extras install docker
              sudo service docker start
              sudo usermod -aG docker ec2-user
              EOF

  tags = {
    Name = "docker-instance"
  }
}

// Create Docker container from the marleneflor/projecte1t:ldap image
resource "docker_container" "ldap" {
  name  = "ldap"
  image = "marleneflor/projecte1t:ldap"
  ports {
    internal = 389
    external = 389
  }
}

// Create Docker container from the brunora/postgres:apache2 image
resource "docker_container" "postgres" {
  name  = "postgres"
  image = "brunora/postgres:apache2"
  ports {
    internal = 5433
    external = 5433
  }
  env = [
    "POSTGRES_PASSWORD = passwd",
    "POSTGRES_DB = training",
]

}

// Create Docker container from the brunora/apache2:v4 image
resource "docker_container" "apache" {
  name  = "apache"
  image = "brunora/apache2:v4"
  ports {
    internal = 80
    external = 80
  }
}

// Expose the EC2 instance's public IP address
output "public_ip" {
  value = aws_instance.example.public_ip
}

