#! /bin/bash

# root dir de los virtualhost 
cp -r web.edt.org /var/www/
cp -r webldap.edt.org /var/www/
cp -r webapirest.edt.org /var/www/

# creamos los virtualhost 
cp virtualhost/* /etc/apache2/sites-available/

# declaramos los virtualhost creando symbolic links de sites-available a sites-enabled
ln -s /etc/apache2/sites-available/web.edt.conf /etc/apache2/sites-enabled/
ln -s /etc/apache2/sites-available/webldap.edt.conf /etc/apache2/sites-enabled/
ln -s /etc/apache2/sites-available/webapirest.edt.conf /etc/apache2/sites-enabled/

# config php
cp php/php.ini /etc/php/7.4/apache2

# SSL/TLS certificate
#mv webapirest.crt /etc/ssl/certs 
#mv webapirest.key /etc/ssl/private

# modulo ssl 
#a2enmod ssl 
# modulo proxy 
#a2enmod proxy
#a2enmod proxy_http

# modulo de autorización y autenticación LDAP en apache2
#a2enmod authnz_ldap

# levantamos applicacion spotify 
#node /var/www/webapirest.edt.org/html/app.js & 

# activamos el servicio 
service apache2 start

# keep container running
tail -f /dev/null 

