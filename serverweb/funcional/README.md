# Servidor web + LDAP Auth + OAuth apirest   

Para realizar los pruebas, hará falta añadir al fichero /etc/hosts del cliente la IP del servidor con los tres subdominios.

### Containers 
```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:ldap

$ docker run --rm --name psql.edt.org -h psql.edt.org -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training --net 2hisx -p 5432:5432 -d brunora/postgres:reto3

```   
## CHECKPOINTS 
Directory [checkpoints](https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE/checkpoints)   
Cada fase del proyecto es aplicada gradualmente segun la version del checkpoint. Creando un entorno de desarrollo con un punto de retorno si al ejecutar algun cambio no termina de funcionar del todo.

## v4 (en desarrollo)  
- Estructura basica del servidor 
- Subdominios 
	- http://web.edt.org 		
		- CSS
	- http://webldap.edt.org	
		- PHP, CSS
		- SQL
		- Autenticación LDAP
	- https://webapirest.edt.org
		- Certificado SSL
		- OAuth Google
		- APIREST
- Base de datos psql
- APIREST  
	- api spotify
	- modulos nodejs
	- http://localhost:8888
	
```
docker run --rm --name serverweb.edt.org -h serverweb.edt.org --net 2hisx -p 80:80 -p 8888:8888 -d brunora/apache2:v4

```
## v3
- Estructura basica del servidor 
- Subdominios 
	- http://web.edt.org 	
	- http://webldap.edt.org	
		- PHP
		- SQL
		- Autenticación LDAP
	- https://webapirest.edt.org	
		- Certificado SSL
		- OAuth Google
- Base de datos psql

```
docker run --rm --name serverweb.edt.org -h serverweb.edt.org --net 2hisx -p 80:80 -p 8888:8888 -d brunora/apache2:v3

```
## v2 
- Estructura basica del servidor 
- Subdominios 
	- http://web.edt.org 	
	- http://webldap.edt.org	
		- PHP
		- SQL
		- Autenticación LDAP
	- https://webapirest.edt.org	
		- Certificado SSL 
- Base de datos psql

```
docker run --rm --name serverweb.edt.org -h serverweb.edt.org --net 2hisx -p 80:80 -p 8888:8888 -d brunora/apache2:v2

```
## v1 
- Estructura basica del servidor 
- Subdominios 
	- http://web.edt.org
	- http://webldap.edt.org 	
		- PHP
		- SQL
	- http://webapirest.edt.org	
- Base de datos psql

```
docker run --rm --name serverweb.edt.org -h serverweb.edt.org --net 2hisx -p 80:80 -p 8888:8888 -d brunora/apache2:v1

```

