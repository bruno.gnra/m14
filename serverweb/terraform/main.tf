
terraform {
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
}
}

provider "aws" {
  region     = "eu-west-2"
  access_key = "AKIAQUYHMRZWWQQXPQ7P"
  secret_key = "wvVNknBrXZTa03IZ5iPlrves5vIgSn32NPrliL2Q"
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = "${aws_instance.example.id}"
  allocation_id = "eipalloc-0711859116a110a3c"
}

resource "aws_instance" "example" {
  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
  instance_type = "t2.small" // replace with your desired instance type
  key_name = "apache"


  tags = {
    Name = "docker-instance"
  }
}


resource "null_resource" "copy_yaml" {
  depends_on = [aws_instance.example]

  provisioner "file" {
    source      = "./install_docker.sh"
    destination = "/home/admin/install_docker.sh"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "13.42.214.216"
    }
  }
}
resource "null_resource" "copy_script" {
  depends_on = [null_resource.copy_yaml]

  provisioner "file" {
    source      = "./compose.yml"
    destination = "/home/admin/compose.yml"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "13.42.214.216"
    }
  }
}
resource "null_resource" "run_script" {
  depends_on = [null_resource.copy_script]

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/admin/install_docker.sh",
      "bash /home/admin/install_docker.sh",
      "sudo docker compose -f /home/admin/compose.yml up -d"
    ]

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "13.42.214.216"
    }
  }
}



// Expose the EC2 instance's public IP address
output "public_ip" {
  value = "13.42.214.216"
}



