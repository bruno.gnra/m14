#Creacion del dominio edt.com AD
Get-windowsFeature
Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
Import-Module ADDSDeployment
Install-ADDSForest -DomainNetbiosName edt.com -SafeModeAdministratorPassword Jupiter123.

#Associacion al servidor DNS freeipa

Set-DnsClientServerAddress -InterfaceIndex 4 -ServerAddresses ("192.168.56.2")
