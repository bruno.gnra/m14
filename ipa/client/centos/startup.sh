#! /bin/bash
dnf update -y

cp /home/vagrant/hosts /etc/hosts
cp /home/vagrant/resolv.conf /etc/resolv.conf

dnf -y install ipa-client
dnf -y install ipa-client-samba
ipa-client-install --unattended --mkhomedir --principal admin --password=jupiter123
ipa-client-samba --unattended


firewall-cmd --add-service={http,https,dns,ntp,freeipa-ldap,freeipa-ldaps,samba,samba-client} --permanent
#sudo firewall-cmd --add-port=137-138/udp --permanent
#sudo firewall-cmd --add-port={139,445}/tcp --permanent
firewall-cmd --reload

systemctl restart smb
systemctl restart winbind

