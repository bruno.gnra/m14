#! /bin/bash

# list users 
users="pere marta anna alice bob mallory" 

#email domain 
domain=$(domainname)

echo -e "jupiter123\n" | kinit admin && echo "OK INIT"

# create users 
echo "*****CREATING USERS******"
 
for user in $users
 do 
   echo -e "k$user\nk$user" | ipa user-add $user --first=$user --last=$user --shel=/bin/bash --email=$user@$domain --password
 done

############GRUPS###################
#list grups 
grups="alumnes profesors" #admins

echo -e "jupiter123\n" | kinit admin && echo "OK INIT"

for grupname in $grups
do
 ipa group-add $grupname
done

echo -e "jupiter123\n" | kinit admin && echo "OK INIT"

# afegir usuaris als grups
ipa group-add-member alumnes --users={marta,pere,anna}
ipa group-add-member profesors --users={bob,alice}
ipa group-add-member admins --users=mallory

exit 0
