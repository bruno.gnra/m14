sudo pdbedit -L



para integrar samba4 con freeipa hace falta configurar una confianza de dominio entre ambos sistemas. 



sudo firewall-cmd --add-service=samba --permanent
sudo firewall-cmd --reload


sudo firewall-cmd --add-port=53/tcp --add-port=88/TCP --add-port=445/tcp --permanent



# Abrir el puerto TCP 53 para DNS
sudo firewall-cmd --add-port=53/tcp --permanent

# Abrir el puerto UDP 53 para DNS
sudo firewall-cmd --add-port=53/udp --permanent

# Abrir el puerto TCP/UDP 88 para Kerberos
sudo firewall-cmd --add-port=88/tcp --permanent
sudo firewall-cmd --add-port=88/udp --permanent

# Abrir el puerto TCP/UDP 389 para LDAP
sudo firewall-cmd --add-port=389/tcp --permanent
sudo firewall-cmd --add-port=389/udp --permanent

# Abrir el puerto TCP/UDP 445 para Samba
sudo firewall-cmd --add-port=445/tcp --permanent
sudo firewall-cmd --add-port=445/udp --permanent


[AD]
sudo yum install ipa-server-trust-ad

ipa-adtrust-install --admin-password=jupiter123 --netbios-name=EDT -add-sids --unattended

WARNING: you MUST re-kinit admin user before using 'ipa trust-*' commands
family in order to re-generate Kerberos tickets to include AD-specific
information


#Abrimos los puertos necesarios en el firewall
sudo firewall-cmd --permanent --add-service=samba
firewall-cmd --reload

# Recargar la configuración del firewall
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
sudo firewall-cmd --list-services

sudo firewall-cmd --list-ports
sudo yum install realmd
sudo realm leave
sudo realm list

# Dominio creado en windows 

echo -e "admin123\n" | ipa trust-add --type=ad edt.edu --admin Administrator --password


sudo vim /etc/samba/smb.conf
sudo systemctl restart smb.service 

sudo dnf install ipa-client
sudo yum install realmd
sudo realm join --user=admin edt.edu --install=/
sudo realm join --user=admin edt.edu --client-software=samba

	security = user
        #encrypt passwords = yes
        winbind use default domain = yes
        idmap config * : range = 2000-9999
        dns forwarder = 192.168.56.2
        
[sincro smb4]

ipa service-add cifs/smb.edt.edu
ipa-getkeytab -s smbfreeipa.edt.edu -p cifs/smb.edt.edu -k /etc/samba/samba.keytab

        
        
#

# TEST IPA 
ipa user-add pere --first=pere --last=pou --email=pere@edt.com --password
echo -e "kvagrant\nkvagrant" ipa user-add vagrant --first=vagrant --last=vagrant --email=vagrant@edt.com --password
echo -e "kanna\nkanna" | ipa user-add anna --first=anna --last=mas --email=anna@edt.com --password

ipa user-find
ipactl status

# CONFIG IPACLIE 
dnf install ipa-client
dnf install ipa-client-samba

ipa-client-install --unattended --mkhomedir --principal admin --password=jupiter123
ipa-client-samba --unattended

systemctl start smb 
systemctl start winbind.service 

# TEST SAMBA-AD
smbclient //smbfreeipa.edt.edu/ -U pere
smbclient -L //192.168.56.2// -U pere
smbclient -k -L //192.168.56.2// 
smbclient -k -L smbfreeipa.edt.edu 


https://www.freeipa.org/page/Howto/Integrating_a_Samba_File_Server_With_IPA
testparm


    freeipa: You must make sure these network ports are open:
    freeipa: 	TCP Ports:
    freeipa: 	  * 135: epmap
    freeipa: 	  * 138: netbios-dgm
    freeipa: 	  * 139: netbios-ssn
    freeipa: 	  * 445: microsoft-ds
    freeipa: 	  * 1024..1300: epmap listener range
    freeipa: 	  * 3268: msft-gc
    freeipa: 	UDP Ports:
    freeipa: 	  * 138: netbios-dgm
    freeipa: 	  * 139: netbios-ssn
    freeipa: 	  * 389: (C)LDAP
    freeipa: 	  * 445: microsoft-ds



