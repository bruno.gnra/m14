> ### 1.2 SERVEI LDAP

>> Desplegar usant un fitxer compose.yml el servei LDAP en una màquina del Cloud. El servei LDAP ha de disposar de dos volums un de dades i un de configuració. També ha de ser programable segons l’argument rebut a l’entrypoint. Ha de permetre inicialitzar de nou la base de dades o simplement engegar el servidor usant la base de dades i les dades existents.

>>>· __initdb:__ *Crea de nou tota la base de dades LDAP.*
>>>
>>>· __start:__ *Inicialitza el servei LDAP usant la configuració i les dades ja existents en els volums.*

>>> #### 1.2.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE1/LDAP*

>>> #### 1.2.1 ARXIUS

>>>> *Haurem de disposar dels arxius __edt-org.ldif__ i del arxiu __slapd.conf__, configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>> #### 1.2.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>> ```
>>>> # Incialitzar la BD ldap segons l'argument introduït i les seves característiques corresponents.
>>>>
>>>> FILE=/var/lib/ldap/.bdcreat
>>>>
>>>> if [ -f "$FILE" ]; then
>>>> 	/usr/sbin/slapd -d0
>>>>
>>>> else
>>>> 	echo "Inicialització BD ldap edt.org"
>>>> 	rm -rf /etc/ldap/slapd.d/*
>>>>	rm -rf /var/lib/ldap/*
>>>>	slaptest -f slapd.conf -F /etc/ldap/slapd.d
>>>>	slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
>>>>	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
>>>>	touch /var/lib/ldap/.bdcreat
>>>>	/usr/sbin/slapd -d0
>>>> fi
>>>> exit 0
>>>> ```

>>> #### 1.2.4 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # ldapserver 2022
>>>>
>>>> FROM debian:latest
>>>> LABEL author="@edt ASIX Curs 2022"
>>>> LABEL subject="ldapserver 2022"
>>>> ARG DEBIAN_FRONTEND=noninteractive
>>>> RUN apt-get update
>>>> RUN apt-get install -y procps iproute2 iputils-ping nmap tree slapd ldap-utils
>>>> RUN mkdir /opt/docker/
>>>> WORKDIR /opt/docker/
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
>>>> EXPOSE 389
>>>> ```

>>> #### 1.2.5 INICIALITZACIÓ CONTAINER

>>>> ##### 1.2.5.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:ldap
>>>>> ```

>>>> ##### 1.2.5.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:ldap
>>>>> ```