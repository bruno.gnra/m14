# If you don't set a default, then you will need to provide the variable
# at run time using the command line, or set it in the environment. For more
# information about the various options for setting variables, see the template
# [reference documentation](https://www.packer.io/docs/templates)
variable "ami_name" {
 type = string
 default = "my-custom-ami"
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

# source blocks configure your builder plugins; your source is then used inside
# build blocks to create resources. A build block runs provisioners and
# post-processors on an instance created by the source.

source "amazon-ebs" "example" {
 access_key = "AKIAQUYHMRZWYXAIF4A2"
 ami_name = "packer ldap ${local.timestamp}"
 instance_type = "t2.micro"
 region = "eu-west-2"
 secret_key = "jYqmYqUHuAkBIj13A0Dw9SboFRJEZxrKCRN+X/F1"
 source_ami_filter {
  filters = {
   name = "debian-11-amd64-20230124-1270"
   root-device-type = "ebs"
   virtualization-type = "hvm"
  }
  most_recent = true
  owners = ["136693071363"]
 }
 ssh_username = "admin"
}
# a build block invokes sources and runs provisioning steps on them.
 build {
    sources = ["source.amazon-ebs.example"]
    
    provisioner "file" {
     destination = "/home/admin/"
     source = "./edt-org.ldif"
    }   
    provisioner "file" {
     destination = "/home/admin/"
     source = "./slapd.conf"
    }
    provisioner "file" {
     destination = "/home/admin/"
     source      = "./ldap.conf"
    }

#    provisioner "shell" {
#     inline = [
#	"sudo apt-get update",
#	"sudo DEBIAN_FRONTEND=noninteractive apt-get install -y nmap tree slapd ldap-utils",
#	"sudo rm -rf /var/lib/ldap/*",
#	"sudo rm -rf /etc/ldap/slapd.d/*",
#	"sudo slaptest -Q -f /home/admin/slapd.conf -F /etc/ldap/slapd.d &> /dev/null || echo OK",
#	"echo OK-slaptest",
#	"sudo slapadd -F /etc/ldap/slapd.d/ -l  /home/admin/edt-org.ldif", 
#	"sudo chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap",
#	"sudo cp /home/admin/ldap.conf /etc/ldap/ldap.conf",
#	"sudo service slapd enable"
#      ]
#     }
}
