sudo pdbedit -L



para integrar samba4 con freeipa hace falta configurar una confianza de dominio entre ambos sistemas. 



sudo firewall-cmd --add-service=samba --permanent
sudo firewall-cmd --reload


sudo firewall-cmd --add-port=53/tcp --add-port=88/TCP --add-port=445/tcp --permanent



# Abrir el puerto TCP 53 para DNS
sudo firewall-cmd --add-port=53/tcp --permanent

# Abrir el puerto UDP 53 para DNS
sudo firewall-cmd --add-port=53/udp --permanent

# Abrir el puerto TCP/UDP 88 para Kerberos
sudo firewall-cmd --add-port=88/tcp --permanent
sudo firewall-cmd --add-port=88/udp --permanent

# Abrir el puerto TCP/UDP 389 para LDAP
sudo firewall-cmd --add-port=389/tcp --permanent
sudo firewall-cmd --add-port=389/udp --permanent

# Abrir el puerto TCP/UDP 445 para Samba
sudo firewall-cmd --add-port=445/tcp --permanent
sudo firewall-cmd --add-port=445/udp --permanent


[AD]
sudo yum install ipa-server-trust-ad

ipa-adtrust-install --admin-password=jupiter123 --netbios-name=EDT -add-sids --unattended

WARNING: you MUST re-kinit admin user before using 'ipa trust-*' commands
family in order to re-generate Kerberos tickets to include AD-specific
information


#Abrimos los puertos necesarios en el firewall
sudo firewall-cmd --permanent --add-service=samba
firewall-cmd --reload

# Recargar la configuración del firewall
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
sudo firewall-cmd --list-services

sudo firewall-cmd --list-ports
sudo yum install realmd
sudo realm leave
sudo realm list

# Dominio creado en windows 

echo -e "admin123\n" | ipa trust-add --type=ad edt.edu --admin Administrator --password


sudo vim /etc/samba/smb.conf
sudo systemctl restart smb.service 

sudo dnf install ipa-client
sudo yum install realmd
sudo realm join --user=admin edt.edu --install=/
sudo realm join --user=admin edt.edu --client-software=samba

	security = user
        #encrypt passwords = yes
        winbind use default domain = yes
        idmap config * : range = 2000-9999
        dns forwarder = 192.168.56.2
        
[sincro smb4]

ipa service-add cifs/smb.edt.edu
ipa-getkeytab -s smbfreeipa.edt.edu -p cifs/smb.edt.edu -k /etc/samba/samba.keytab

        
        
#

# TEST IPA 
echo -e "kpere\nkpere" | ipa user-add pere --first=pere --last=pou --email=pere@edt.com --password
echo -e "kvagrant\nkvagrant" ipa user-add vagrant --first=vagrant --last=vagrant --email=vagrant@edt.com --password
echo -e "kanna\nkanna" | ipa user-add anna --first=anna --last=mas --email=anna@edt.com --password

ipa user-find
ipactl status

ipa user-add Administrator --first=Administrator --last=Administrator --email=Administrator@edt.com --password
echo -e "kvagrant\nkvagrant" ipa user-add vagrant --first=vagrant --last=vagrant -

# CONFIG IPACLIE 
dnf install ipa-client
dnf install ipa-client-samba

ipa-client-install --unattended --mkhomedir --principal admin --password=jupiter123
ipa-client-samba --unattended

systemctl start smb 
systemctl start winbind.service 

# TEST SAMBA-AD
smbclient //smbfreeipa.edt.edu/ -U pere
smbclient -L //192.168.56.2// -U pere
smbclient -k -L //192.168.56.2// 
smbclient -k -L smbfreeipa.edt.edu 

smbclient -L //192.168.56.102// -U anna #test server windows

# POWERSHELL IPS
New-NetIPAddress -IPAddress 10.0.2.15 -DefaultGateway 10.0.2.2 -PrefixLength 24 -InterfaceAlias "Ethernet"

New-NetIPAddress -IPAddress 192.168.56.2 -DefaultGateway 192.168.56.1 -PrefixLength 24 -InterfaceAlias "Ethernet 2"

Set-NetIPAddress -InterfaceAlias "Ethernet" - -IPAddress 10.0.2.15 -PrefixLength 24

Set-NetIPAddress -InterfaceAlias "Ethernet 2" -AddressFamily IPv4 -IPAddress 192.168.57.2 -PrefixLength 24

New-NetRoute -DestinationPrefix "0.0.0.0/0" -InterfaceAlias "Ethernet 2" -NextHop "192.168.56.1"

Get-NetAdapter

#[POWERSHELL DNS]

Set-DnsClientServerAddress -InterfaceAlias "Ethernet 2" -ServerAddresses ("192.168.57.2","192.168.56.2")
Get-DnsClientServerAddress


# MODO ...
Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools

Install-WindowsFeature -Name RSAT-AD-PowerShell # opcional
Import-Module ADDSDeployment


Install-ADDSForest -DomainName "ad.edt.edu" -NewDomainNetbiosName EDT -DomainMode "Win2012R2" -ForestMode "Win2012R2" -InstallDNS -SafeModeAdministratorPassword (ConvertTo-SecureString -String "jupiter123." -AsPlainText -Force) -Force

ssh -p 2222 Administrator@localhost
dnscmd 127.0.0.1 /RecordAdd edt.edu smb.ipa A 192.168.56.2
dnscmd 127.0.0.1 /RecordAdd edt.edu ipa NS smb.ipa.edt.edu
dnscmd 127.0.0.1 /ClearCache


ipa dnsforwardzone-add edt.edu  --forwarder=192.168.56.4 --forward-policy=only
ipa trust-add --type=ad ad.edt.edu --admin Administrator --password


https://www.freeipa.org/page/Howto/Integrating_a_Samba_File_Server_With_IPA
testparm


    freeipa: You must make sure these network ports are open:
    freeipa: 	TCP Ports:
    freeipa: 	  * 135: epmap
    freeipa: 	  * 138: netbios-dgm
    freeipa: 	  * 139: netbios-ssn
    freeipa: 	  * 445: microsoft-ds
    freeipa: 	  * 1024..1300: epmap listener range
    freeipa: 	  * 3268: msft-gc
    freeipa: 	UDP Ports:
    freeipa: 	  * 138: netbios-dgm
    freeipa: 	  * 139: netbios-ssn
    freeipa: 	  * 389: (C)LDAP
    freeipa: 	  * 445: microsoft-ds
    
    
        freeipa: 	1. You must make sure these network ports are open:
    freeipa: 		TCP Ports:
    freeipa: 		  * 80, 443: HTTP/HTTPS
    freeipa: 		  * 389, 636: LDAP/LDAPS
    freeipa: 		  * 88, 464: kerberos
    freeipa: 		  * 53: bind
    freeipa: 		UDP Ports:
    freeipa: 		  * 88, 464: kerberos
    freeipa: 		  * 53: bind
    freeipa: 		  * 123: ntp
    freeipa: 
	q


[global]
    # Limit number of forked processes to avoid SMBLoris attack
    max smbd processes = 1000
    # Use dedicated Samba keytab. The key there must be synchronized
    # with Samba tdb databases or nothing will work
    dedicated keytab file = FILE:/etc/samba/samba.keytab
    kerberos method = dedicated keytab
    # Set up logging per machine and Samba process
    log file = /var/log/samba/log.%m
    log level = 1
    # We force 'member server' role to allow winbind automatically
    # discover what is supported by the domain controller side
    server role = member server
    realm = EDT.EDU
    netbios name = CENTOS
    workgroup = EDT
    # Local writable range for IDs not coming from IPA or trusted domains
    idmap config * : range = 0 - 0
    idmap config * : backend = tdb


    idmap config EDT : range = 1321000000 - 1321199999
    idmap config EDT : backend = sss


# Default homes share
[homes]
    read only = no



