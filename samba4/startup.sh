#! /bin/bash 

dnf -y update
# instalamos paquetes samba4, ipa-client 
dnf install -y samba*
dnf install -y sssd-*
dnf install -y ipa-client

cp /home/vagrant/hosts /etc/hosts
cp /home/vagrant/resolv.conf /etc/resolv.conf
#ipa-client-install --unattended --domain=edt.edu --server=smbfreeipa.edt.edu --realm=EDT.EDU --mkhomedir

#Abrimos los puertos necesarios en el firewall
#sudo firewall-cmd --permanent --add-service=samba
#firewall-cmd --reload

# restart and check
#sudo systemctl restart smb.service
#sudo systemctl restart nmb.service
#sudo systemctl status smb.service
#smbd --version
#pdbedit -L
