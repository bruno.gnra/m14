
terraform {
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
}
}

provider "aws" {
  region     = "eu-west-2"
  access_key = "AKIAQUYHMRZWYXAIF4A2"
  secret_key = "jYqmYqUHuAkBIj13A0Dw9SboFRJEZxrKCRN+X/F1"
}

resource "aws_instance" "example" {
  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
  instance_type = "t2.small" // replace with your desired instance type
  key_name = "apache"


  tags = {
    Name = "docker-instance"
  }
}
resource "null_resource" "copy_yaml" {
  depends_on = [aws_instance.example]

  provisioner "file" {
    source      = "./install_docker.sh"
    destination = "/home/admin/install_docker.sh"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = aws_instance.example.public_ip
    }
  }
}
resource "null_resource" "copy_script" {
  depends_on = [null_resource.copy_yaml]

  provisioner "file" {
    source      = "./compose.yml"
    destination = "/home/admin/compose.yml"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = aws_instance.example.public_ip
    }
  }
}
resource "null_resource" "run_script" {
  depends_on = [null_resource.copy_script]

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/admin/install_docker.sh",
      "bash /home/admin/install_docker.sh",
      "sudo docker compose -f /home/admin/compose.yml up -d"
    ]

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = aws_instance.example.public_ip
    }
  }
}



// Expose the EC2 instance's public IP address
output "public_ip" {
  value = aws_instance.example.public_ip
}


